/*
Wordcheat by Kevin Zhang. Copyright 2014.
sudo@pt-get.com
*/

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <map>
#include <set>
#include <cctype>
#include <cmath>

namespace kouver {
	namespace wordcheat {
		using uint = unsigned int;
		using coord = std::pair < uint, uint > ;
		using scoord = std::pair < int, int > ;
		inline scoord tosigned(coord i) { return scoord(i.first, i.second); }
		inline int lensq(const coord& one, const coord& two = coord(0, 0)) {
			return (one.first - two.first) * (one.first - two.first) +
				(one.second - two.second) * (one.second - two.second);
		}
		inline scoord minus(const coord& one, const coord& two) {
			return coord((one.first - two.first), (one.second - two.second));
		}

		class dictionary {
		private:
			size_t _size;
			char _letter;
			std::vector<dictionary*> _next;
		public:
			dictionary(char letter = '\0') : _letter(letter) {
				_next = std::vector<dictionary*>();
				_next.resize(27);
			}
			~dictionary() {
				for (dictionary* d : _next) {
					if (d != nullptr)
						delete d;
				}
			}
			bool has(std::string word, int iteration = -1) {
				if (iteration + 1 < (signed)word.length()) {
					if (_next[word[iteration + 1] - 'a'] == nullptr)
						return false;
					return _next[word[iteration + 1] - 'a']->has(word, iteration + 1);
				}
				return _next['z' - 'a' + 1] != nullptr;
			}
			bool has(char next) {
				return _next[tolower(next) - 'a'] != nullptr;
			}
			dictionary* get(char next) {
				return _next[tolower(next) - 'a'];
			}
			void word(std::string word, int iteration = -1) {
				if (iteration + 1 < (signed)word.length()) {
					if (_next[word[iteration + 1] - 'a'] == nullptr)
						_next[word[iteration + 1] - 'a'] = new dictionary(word[iteration + 1]);
					_next[word[iteration + 1] - 'a']->word(word, iteration + 1);
				}
				else
					_next['z' - 'a' + 1] = new dictionary('z' - 'a' + 1);
			}
			/*
			Populates a dictionary from an input stream. Words should be placed one per line.
			Words with non-letter characters are ignored.
			*/
			void populate(std::istream* in, std::ostream* out) {
				_size = 0;
				std::string line;

				while (*in >> line) {
					bool good = true;

					//Check that they are all lowercase.
					//Proper nouns are not allowed.
					for (char c : line) {
						if (!((c >= 'a' && c <= 'z')))
							good = false;
					}

					if (line == "")
						good = false;

					if (!good)
						continue;

					//Add word to dictionary.
					word(line);
					_size++;
				}
			}
			size_t size() {
				return _size;
			}
		};

		//Represents an entry that will be return as a mapped value.
		struct entry {
			std::string word;
			std::vector<coord> path;
			scoord vector;
			int maxhoriz;
			int maxvert, minvert;

			entry(std::string word, std::vector<coord> path) :
				word(word), path(std::vector<coord>(path.begin(), path.end())) {

				vector = minus(path.back(), path.front());
				for (const coord& a : path)
					if (lensq(a, path.front()) > lensq(vector))
						vector = minus(a, path.front());

				maxhoriz = vector.first;
				for (const coord& a : path)
					if (abs((signed) a.first - (signed) path.front().first) > maxhoriz)
						maxhoriz = abs((signed) a.first - (signed) path.front().first);

				maxvert = (signed) path.front().second + vector.second;
				minvert = maxvert;
				for (const coord& a : path)
					if ((signed) a.second + (signed) path.front().second > maxvert)
						maxvert = (signed) a.second + (signed) path.front().second;
					else if ((signed) a.second + (signed) path.front().second < minvert)
						minvert = (signed) a.second + (signed) path.front().second;
			}
		};
		struct entry_cmp {
			inline bool operator() (const entry& first, const entry& second) {
				return first.word.length() > second.word.length();
			}
		};
		struct strlen_cmp {
			inline bool operator() (const std::string& first, const std::string& second) {
				if (first.length() > second.length())
					return true;
				if (first.length() < second.length())
					return false;
				return first > second;
			}
		};

		typedef std::map<std::string, std::vector<entry>, strlen_cmp> result;
		typedef std::pair<std::string, std::vector<entry>> result_entry;

		class board {
		private:
			std::vector<std::string> _internal;
			//Recursively match words.
			void match(dictionary* parallel, coord position, std::vector<coord>* path, std::vector<entry>* entries) {
				if (parallel->has('z' + 1)) {
					std::string s;
					for (coord c : *path)
						s += tolower(get(c));
					entries->push_back(entry(s, *path));
				}

				std::vector<coord> neighbours;

				if (position.first > 0)
					neighbours.push_back(coord(position.first - 1, position.second));
				if (position.first < x - 1)
					neighbours.push_back(coord(position.first + 1, position.second));
				if (position.second > 0)
					neighbours.push_back(coord(position.first, position.second - 1));
				if (position.second < y - 1)
					neighbours.push_back(coord(position.first, position.second + 1));

				if (position.first > 0 && position.second > 0)
					neighbours.push_back(coord(position.first - 1, position.second - 1));
				if (position.first > 0 && position.second < y - 1)
					neighbours.push_back(coord(position.first - 1, position.second + 1));
				if (position.first < x - 1 && position.second > 0)
					neighbours.push_back(coord(position.first + 1, position.second - 1));
				if (position.first < x - 1 && position.second < y - 1)
					neighbours.push_back(coord(position.first + 1, position.second + 1));

				for (coord c : neighbours) {
					bool cycle = false;
					for (coord t : *path)
						if (t.first == c.first && t.second == c.second)
							cycle = true;
					if (cycle)
						continue;

					if (parallel->has(get(c))) {
						path->push_back(c);
						match(parallel->get(get(c)), c, path, entries);
						path->pop_back();
					}
				}
			}
		public:
			uint x, y;
			board(uint x, uint y) : x(x), y(y) {
				_internal = std::vector<std::string>(y);
			}
			void make(std::istream* input, std::ostream* output) {
				for (uint i = 0; i < y; i++) {
					bool good = true;
					std::string line;

					do {
						good = true;
						*input >> line;

						if (line.length() != x) {
							*output << "Invalid file. Row size should be " << x << "." << std::endl;
							return;
						}

						for (char c : line)
							if (!((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')))
								good = false;

						if (!good)
							*output << "Invalid character." << std::endl;
					} while (!good);

					_internal[i] = line;
				}
			}

			char get(coord p) {
				if (p.first >= this->x || p.second >= this->y)
					return '\0';
				return _internal[p.second][p.first];
			}

			result work(dictionary* root) {
				std::vector<entry> entries;
				std::vector<coord> start;

				for (uint i = 0; i < x; i++)
					for (uint o = 0; o < y; o++)
						if (get(coord(i, o)) >= 'A' && get(coord(i, o)) <= 'Z')
							start.push_back(coord(i, o));

				for (coord s : start) {
					std::vector<coord> path;
					path.push_back(s);
					if (root->has(get(s)))
						this->match(root->get(get(s)), s, &path, &entries);
				}

				std::sort(entries.begin(), entries.end(), entry_cmp());

				result results;

				for (entry e : entries)
					results[e.word].push_back(e);

				return results;
			}
		};

		class wordcheater {
		private:
			std::vector<std::string> stresults;
			size_t showlower;
			size_t showamt = 10;
		public:
			dictionary dict;
			board brd;
			result results;

			wordcheater(std::string words, std::string b) : brd(0, 0) {
				repopulate(words);
				rebuild(b);
				make();

				while (true) {
					std::string line;
					std::cin >> line;
					std::cout << std::endl;

					if (line == "rebuild") {
						rebuild(b);
					}
					else if (line == "rebuildfrom") {
						std::string file;
						std::cin >> file;
						rebuild(file);
					}
					else if (line == "repopulate") {
						repopulate(words);
					}
					else if (line == "make") {
						if (dict.size() < 1) {
							std::cout << "No dictionary loaded. Please type 'repopulate'."
								<< std::endl;
							continue;
						}
						if (brd.x < 1 || brd.y < 1) {
							std::cout << "No board loaded. Please type 'rebuild'."
								<< std::endl;
							continue;
						}

						make();
					}
					else if (line == "more") {
						if (stresults.size() < 1) {
							std::cout << "No solutions generated yet. Please type 'make'."
								<< std::endl;
							continue;
						}

						if (showlower + showamt < stresults.size())
							showlower += showamt;
						show(showlower, showlower + showamt);
					}
					else if (line == "less") {
						if (stresults.size() < 1) {
							std::cout << "No solutions generated yet. Please type 'make'."
								<< std::endl;
							continue;
						}

						if (showlower > 0)
							showlower = std::max((size_t)0, showlower - showamt);
						show(showlower, showlower + showamt);
					}
					else if (line == "show") {
						if (stresults.size() < 1) {
							std::cout << "No solutions generated yet. Please type 'make'."
								<< std::endl;
							continue;
						}

						int upper, lower;
						std::cin >> lower >> upper;
						show(lower - 1, upper);
					}
					else if (line == "how") {
						if (stresults.size() < 1) {
							std::cout << "No solutions generated yet. Please type 'make'." << std::endl;
							continue;
						}

						std::string word;
						std::cin >> word;
						if (results.find(word) != results.end()) {
							std::vector<entry>& paths = results[word];
							std::vector<coord>* p = &paths.front().path;

							if (paths.size() > 1) {
								std::cout << "\"" << word << "\"" << " has multiple solutions."
									<< std::endl;
								std::cout << "Choose the desired starting point:" << std::endl;

								for (uint o = 0; o < brd.y; o++) {
									for (uint i = 0; i < brd.x; i++) {
										size_t index;
										for (index = 0; index < paths.size() &&
											paths[index].path.front() != coord(i, o); index++);

											if (index < paths.size())
												std::cout << (char)(index + 'A');
											else
												std::cout << '.';
									}
									std::cout << std::endl;
								}

								size_t ind = -1;
								char s;
								std::cin >> s;
								ind = tolower(s) - 'a';

								while (ind > paths.size()) {
									std::cout << "Invalid starting point." << std::endl;
									std::cin >> s;
									ind = tolower(s) - 'a';
								}

								p = &paths[ind].path;
							}

							std::set<coord> places(p->begin(), p->end());

							for (uint o = 0; o < brd.y; o++) {
								for (uint i = 0; i < brd.x; i++) {
									if (places.count(coord(i, o)) > 0)
										std::cout << (char)(p->front() == coord(i, o) ?
										toupper(brd.get(coord(i, o))) : tolower(brd.get(coord(i, o))));
									else
										std::cout << '.';
								}
								std::cout << std::endl;
							}
						}
						else {
							std::cout << "\"" << word << "\"" << " does not have a solution."
								<< std::endl;
						}
					}
					else if (line == "analyze") {
						if (stresults.size() < 1) {
							std::cout << "No solutions generated yet. Please type 'make'."
								<< std::endl;
							continue;
						}


						std::vector<entry*> sorted_len;
						for (auto it = results.begin(); it != results.end(); it++) {
							std::sort(it->second.begin(), it->second.end(),
								[](const entry& first, const entry& second) {
								return lensq(first.vector) > lensq(second.vector);
							});
						}
						for (auto it = results.begin(); it != results.end(); it++)
							sorted_len.push_back(&it->second.front());

						std::sort(sorted_len.begin(), sorted_len.end(),
							[](const entry* first, const entry* second) {
							return lensq(first->vector) > lensq(second->vector);
						});

						std::cout << "Word vectors with longest length:" << std::endl;
						for (size_t i = 0; i < std::min(sorted_len.size(), (size_t)5); i++) {
							std::cout << sqrt(lensq(sorted_len[i]->vector)) << " - " <<
								sorted_len[i]->word << std::endl;
						}

						bool top = isupper(brd.get(coord(0, 0))) != 0;
						std::vector<entry*> sorted_vert;
						for (auto it = results.begin(); it != results.end(); it++) {
							std::sort(it->second.begin(), it->second.end(),
								[top](const entry& first, const entry& second) {
								if (top)
									return first.maxvert > second.maxvert;
								return first.minvert < second.minvert;
							});
						}

						for (auto it = results.begin(); it != results.end(); it++)
							sorted_vert.push_back(&it->second.front());

						std::sort(sorted_vert.begin(), sorted_vert.end(),
							[top](const entry* first, const entry* second) {
							if (top)
								return first->maxvert > second->maxvert;
							return first->minvert < second->minvert;
						});

						std::cout << "Word vectors with longest vertical reach:" << std::endl;
						for (size_t i = 0; i < std::min(sorted_vert.size(), (size_t)5); i++) {
							std::cout << (top ? sorted_vert[i]->maxvert : brd.y - sorted_vert[i]->minvert)
								<< " - " << sorted_vert[i]->word << std::endl;
						}
						std::vector<entry*> sorted_horiz;
						for (auto it = results.begin(); it != results.end(); it++) {
							std::sort(it->second.begin(), it->second.end(),
								[](const entry& first, const entry& second) {
								return first.maxhoriz > second.maxhoriz;
							});
						}

						for (auto it = results.begin(); it != results.end(); it++)
							sorted_horiz.push_back(&it->second.front());

						std::sort(sorted_horiz.begin(), sorted_horiz.end(),
							[](const entry* first, const entry* second) {
							return first->maxhoriz > second->maxhoriz;
						});

						std::cout << "Word vectors with longest horizontal reach:" << std::endl;
						for (size_t i = 0; i < std::min(sorted_horiz.size(), (size_t)5); i++) {
							std::cout << sorted_horiz[i]->maxhoriz <<
								" - " << sorted_horiz[i]->word << std::endl;
						}
					}
					else if (line == "help") {
						std::cout <<
							"Wordcheat by Kevin Zhang." << std::endl <<
							"Cheating tool for the game Wordbase." << std::endl <<
							"Commands:" << std::endl <<
							"help - What do you think it does?" << std::endl <<
							"make - Recalculate solutions." << std::endl <<
							"repopulate - Repopulate dictionary from file 'words.txt'." << std::endl <<
							"rebuild - Rebuild board from file 'board.txt'." << std::endl <<
							"rebuildfrom X - Rebuild board from file X." << std::endl <<
							"more - Show next 10 solutions." << std::endl <<
							"less - Show previous 10 solutions." << std::endl <<
							"show X Y - Show solutions X to Y." << std::endl <<
							"how X - Show specific solution to word X." << std::endl <<
							"analyze - Find best offensive and defensive words." << std::endl
							;
					}
					else {
						std::cin.clear();
						std::cout << "Unknown command." << std::endl;
					}
				}
			}

			void repopulate(std::string wordfile) {
				std::cout << "Populating wordtree." << std::endl;
				std::ifstream in(wordfile);
				dict.populate(&in, &std::cout);
				in.close();
				std::cout << dict.size() << " words found." << std::endl;
			}
			void rebuild(std::string cheat) {
				std::cout << "Building gameboard." << std::endl;
				std::ifstream in(cheat);
				int w, h;
				in >> w >> h;
				if (w < 1 || h < 1) {
					std::cout << "Invalid width and height." << std::endl;
				}

				brd = board(w, h);
				brd.make(&in, &std::cout);
				in.close();

				std::cout << w << "x" << h << " board built." << std::endl;
			}
			void make() {
				std::cout << "Finding moves." << std::endl << std::endl;
				results = brd.work(&dict);
				std::cout << results.size() << " moves found." << std::endl;

				stresults.clear();
				for (result_entry e : results)
					stresults.push_back(e.first);

				showlower = 0;
				show(showlower, showlower + showamt);
			}
			void show(size_t lower, size_t upper) {
				if (lower < 0)
					lower = 0;
				if (upper >= stresults.size())
					upper = stresults.size() - 1;

				std::cout << "Showing results " << lower + 1 << " to " << upper << " of "
					<< stresults.size() << "." << std::endl;

				for (size_t i = lower; i < upper; i++)
					std::cout << stresults[i] << std::endl;
			}
		};
	}
}

namespace kw = kouver::wordcheat;

int main() {
	std::string words = "words.txt";
	std::string board = "board.txt";

	kw::wordcheater(words, board);
}
