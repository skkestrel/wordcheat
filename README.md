wordcheat
=========
Cheating tool for the mobile game Wordbase.

Calculates your best moves.

Requires two files:

words.txt - a dictionyary of words seperated by newlines (eg. /usr/share/dict/words)

board.txt - the board layout

How board.txt should be formatted:

```
4 6
abcd
abcd
abcd
ABCD
ABCD
ABCD
```

This board has width 4 and height 6. Allowed starting locations should be marked by capital letters.
Standard Wordbase boards are 10 width by 13 height.

Type "help" for a list of commands while inside the executable.
